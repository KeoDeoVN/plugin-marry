package mk.plugin.marry.manager;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.marry.data.DataUtils;
import mk.plugin.marry.data.PMData;
import mk.plugin.marry.item.Items;
import mk.plugin.marry.lang.Lang;
import mk.plugin.marry.object.Proposal;

public class MarryProposals {
	
	private static Map<Player, Proposal> proposals = Maps.newHashMap();
	
	public static Map<Player, Proposal> getProposals() {
		return proposals;
	}
	
	public static boolean hasRing(Player player) {
		for (ItemStack i : player.getInventory().getContents()) {
			if (Items.isProposalRing(i)) return true;
		}
		return false;
	}
	
	public static void takeRing(Player player) {
		ItemStack[] contents = player.getInventory().getContents();
		for (int i = 0 ; i < contents.length ; i++) {
			ItemStack item = contents[i];
			if (Items.isProposalRing(item)) {
				if (item.getAmount() > 1) item.setAmount(item.getAmount() - 1);
				else contents[i] = null;
			}
		}
		player.getInventory().setContents(contents);
		player.updateInventory();
	}
	
	public static boolean isMarried(Player player) {
		PMData pd = DataUtils.getData(player);
		if (pd.getPartner() != null) {
			return true;
		}
		return false;
	}
	
	public static boolean hasProposal(Player player) {
		return proposals.containsKey(player);
	}
	
	public static boolean sentProposal(Player player) {
		for (Proposal p : proposals.values()) {
			if (p.getSender().equals(player.getName())) return true;
		}
		return false;
	}
	
	public static void send(Player player, Player target, String message) {
		// Check conditions
		if (player == target) {
			Lang.PROPOSAL_YOURSELF.send(player);
			return;
		}
		if (isMarried(player)) {
			Lang.MARRY_ALREADY.send(player);
			return;
		}
		if (isMarried(target)) {
			Lang.MARRY_ALREADY.send(player);
			return;
		}
		if (hasProposal(player)) {
			Lang.PROPOSAL_SENDER_ALREADY_HAD.send(player);
			return;
		}
		if (hasProposal(target)) {
			Lang.PROPOSAL_TARGET_ALREADY_HAD.send(player);
			return;
		}
		if (sentProposal(player)) {
			Lang.PROPOSAL_SENDER_ALREADY_SENT.send(player);
			return;
		}
		if (sentProposal(target)) {
			Lang.PROPOSAL_TARGET_ALREADY_SENT.send(player);
			return;
		}
		
		// Take ring
		takeRing(player);
		
		// Send
		Proposal p = new Proposal(player.getName());
		proposals.put(target, p);
		
		// Messages
		target.sendTitle(Lang.PROPOSAL_TITLE.get(), Lang.PROPOSAL_SUBTITLE.get(), 10, 40, 10);
		target.sendMessage(Lang.PROPOSAL_MESSAGE.get().replace("%player%", player.getName()).replace("%message%", message));
		Lang.broadcast(Lang.PROPOSAL_BROADCAST.get().replace("%player%", player.getName()).replace("%target%", target.getName()));
	}
	
	public static void agree(Player player) {
		if (!hasProposal(player)) {
			Lang.PROSOSAL_NOT_HAVE.send(player);
			return;
		}
		Proposal p = proposals.get(player);
		proposals.remove(player);
		
		PMData pd = DataUtils.getData(player);
		String sender = p.getSender();
		pd.setPartner(sender);
		pd.save();
		
		pd = DataUtils.getData(sender);
		pd.setPartner(player.getName());
		pd.save();
		
		// Give ring, message
		player.getInventory().addItem(Items.getEngagementRing());
		if (Bukkit.getPlayer(sender) != null) {
			Player pl = Bukkit.getPlayer(sender);
			pl.getInventory().addItem(Items.getEngagementRing());
			pl.sendTitle(Lang.ENGAGEMENT_TITLE.get(), Lang.ENGAGEMENT_SUBTITLE.get(), 10, 40, 10);
			pl.sendMessage(Lang.ENGAGEMENT_MESSAGE.get());
		}
		player.sendTitle(Lang.ENGAGEMENT_TITLE.get(), Lang.ENGAGEMENT_SUBTITLE.get(), 10, 40, 10);
		player.sendMessage(Lang.ENGAGEMENT_MESSAGE.get());
		
		Lang.broadcast(Lang.ENGAGEMENT_BROADCAST.get().replace("%player1%", player.getName()).replace("%player2%", sender));
	}
	
	public static void refuse(Player player) {
		if (!hasProposal(player)) {
			Lang.PROSOSAL_NOT_HAVE.send(player);
			return;
		}
		Proposal p = proposals.get(player);
		String sender = p.getSender();
		proposals.remove(player);
		
		Lang.PROPOSAL_REFUSE_TARGET_MESSAGE.send(player);
		Lang.broadcast(Lang.PROPOSAL_REFUSE_BROADCAST.get().replace("%sender%", sender).replace("%target%", player.getName()));
		
		Player target = Bukkit.getPlayer(sender);
		if (target != null) {
			Lang.PROPOSAL_REFUSE_SENDER_MESSAGE.send(target);
			target.sendTitle(Lang.PROPOSAL_REFUSE_SENDER_TITLE.get(), Lang.PROPOSAL_REFUSE_SENDER_SUBTITLE.get(), 10, 40, 10);
		}

	}
	
}
