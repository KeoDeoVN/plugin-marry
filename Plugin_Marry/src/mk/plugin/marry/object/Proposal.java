package mk.plugin.marry.object;

import mk.plugin.marry.config.Configs;

public class Proposal {
	
	private String sender;
	private long timeExpired;
	
	public Proposal(String sender) {
		this.sender = sender;
		this.timeExpired = System.currentTimeMillis() + Configs.TIME_WAIT * 1000;
	}
	
	public String getSender() {
		return this.sender;
	}
	
	public long getTimeExpired() {
		return this.timeExpired;
	}
	
}
