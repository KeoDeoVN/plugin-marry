package mk.plugin.marry.item;

import org.bukkit.inventory.ItemStack;

import de.tr7zw.itemnbtapi.NBTItem;
import mk.plugin.marry.config.Configs;
import mk.plugin.niceshops.storage.ItemStorage;

public class Items {
	
	public static ItemStack getProposalRing() {
		ItemStack item = ItemStorage.get(Configs.ITEM_PROPOSAL_RING);
		NBTItem nbti = new NBTItem(item);
		nbti.setString("marry-proposalRing", "ok");
		return nbti.getItem(); 
	}
	
	public static boolean isProposalRing(ItemStack i) {
		if (i == null) return false;
		return new NBTItem(i).hasKey("marry-proposalRing");
	}
	
	public static ItemStack getEngagementRing() {
		ItemStack item = ItemStorage.get(Configs.ITEM_ENGAGEMENT_RING);
		NBTItem nbti = new NBTItem(item);
		nbti.setString("marry-engagementRing", "ok");
		return nbti.getItem();
	}
	
	public static boolean isEngagementRing(ItemStack i) {
		if (i == null) return false;
		return new NBTItem(i).hasKey("marry-engagementRing");
	}
	
	public static ItemStack getDivorcePaper() {
		ItemStack item = ItemStorage.get(Configs.ITEM_DIVORCE_PAPER);
		NBTItem nbti = new NBTItem(item);
		nbti.setString("marry-divorcePaper", "ok");
		return nbti.getItem();
	}
	
	public static boolean isDivorcePaper(ItemStack i) {
		if (i == null) return false;
		return new NBTItem(i).hasKey("marry-divorcePaper");
	}
	
}
