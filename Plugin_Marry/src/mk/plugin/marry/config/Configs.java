package mk.plugin.marry.config;

import org.bukkit.configuration.file.FileConfiguration;

public class Configs {
	
	public static String ITEM_PROPOSAL_RING;
	public static String ITEM_ENGAGEMENT_RING;
	public static String ITEM_DIVORCE_PAPER;
	
	public static int TIME_WAIT = 30;
	
	public static void reload(FileConfiguration config) {
		ITEM_PROPOSAL_RING = config.getString("item.proposal-ring");
		ITEM_ENGAGEMENT_RING = config.getString("item.engagement-ring");
		ITEM_DIVORCE_PAPER = config.getString("item.divorce-paper");
		
		if (config.contains("time-wait")) {
			TIME_WAIT = config.getInt("time-wait");
		} else config.set("time-wait", TIME_WAIT);
	}
	
}
