package mk.plugin.marry.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.marry.data.DataUtils;
import mk.plugin.marry.data.PMData;
import mk.plugin.marry.item.Items;
import mk.plugin.marry.lang.Lang;
import mk.plugin.marry.util.MarryUtils;

public class MarryDivorces {
	
	public static boolean hasPaper(Player player) {
		for (ItemStack i : player.getInventory().getContents()) {
			if (Items.isDivorcePaper(i)) return true;
		}
		return false;
	}
	
	public static void takePaper(Player player) {
		ItemStack[] contents = player.getInventory().getContents();
		for (int i = 0 ; i < contents.length ; i++) {
			ItemStack item = contents[i];
			if (Items.isDivorcePaper(item)) {
				if (item.getAmount() > 1) item.setAmount(item.getAmount() - 1);
				else contents[i] = null;
			}
		}
		player.getInventory().setContents(contents);
		player.updateInventory();
	}
	
	public static void divorce(Player player) {
		if (!MarryUtils.isMarried(player)) {
			player.sendMessage(Lang.DIVORCE_NOT_MARRIED.get());
			return;
		}
		takePaper(player);
		
		PMData pd = DataUtils.getData(player);
		String partner = pd.getPartner();
		pd.setPartner(null);
		pd.save();
		
		pd = DataUtils.getData(partner);
		pd.setPartner(null);
		pd.save();
		
		player.sendTitle(Lang.DIVORCE_TITLE.get(), Lang.DIVORCE_SUBTITLE.get(), 10, 40, 10);
		Lang.broadcast(Lang.DIVORCE_BROADCAST.get().replace("%player1%", player.getName()).replace("%player2%", partner));
		
		if (Bukkit.getPlayer(partner) != null) {
			player = Bukkit.getPlayer(partner);
			player.sendTitle(Lang.DIVORCE_TITLE.get(), Lang.DIVORCE_SUBTITLE.get(), 10, 40, 10);
		}
	}
	
}
