package mk.plugin.marry.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.marry.manager.MarryDivorces;
import mk.plugin.marry.manager.MarryProposals;

public class PlayerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		Player player = (Player) sender;
		
		// Agree
		if (cmd.getName().equalsIgnoreCase("emdongy")) {
			MarryProposals.agree(player);
		}
		
		else if (cmd.getName().equalsIgnoreCase("emxinloi")) {
			MarryProposals.refuse(player);
		}
		
		else if (cmd.getName().equalsIgnoreCase("lyhon")) {
			if (MarryDivorces.hasPaper(player)) {
				MarryDivorces.divorce(player);
			} else player.sendMessage(ChatColor.RED + "Bạn phải có giấy ly hôn");
		}
		
		else if (cmd.getName().equalsIgnoreCase("cauhon")) {
			try {
				// Check
				Player target = Bukkit.getPlayer(args[0]);
				if (target == null) {
					sender.sendMessage(ChatColor.DARK_RED + "Không tìm thấy đối tượng");
					return false;
				}
				String message = "";
				for (int i = 1 ; i < args.length ; i++) {
					message += args[i] + " ";
				}
				if (message.length() > 0) message = message.substring(0, message.length() - 1);
				
				// Send
				if (MarryProposals.hasRing(player)) {
					MarryProposals.send(player, target, message.replace("&", "§"));
				} else player.sendMessage(ChatColor.RED + "Bạn phải có nhẫn cầu hôn");

			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage(ChatColor.GREEN + "Cầu hôn ghi: /cauhon <player> <message>. Ví dụ: /cauhon MasterClaus Anh yeu em <3");
			}
			
		}
		
		return false;
	}

	
	
}
