package mk.plugin.marry.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.marry.item.Items;
import mk.plugin.marry.main.MainMarry;
import mk.plugin.marry.manager.MarryDivorces;
import net.md_5.bungee.api.ChatColor;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
			
		try {
			if (args[0].equalsIgnoreCase("reload")) {
				MainMarry.getPlugin(MainMarry.class).reloadConfigs();
				sender.sendMessage("Reloaded");
			}
			else if (args[0].equalsIgnoreCase("get")) {
				Player player = (Player) sender;
				if (args[1].equalsIgnoreCase("proposal")) {
					player.getInventory().addItem(Items.getProposalRing());
				}
				else if (args[1].equalsIgnoreCase("engagement")) {
					player.getInventory().addItem(Items.getEngagementRing());
				}
				else if (args[1].equalsIgnoreCase("divorce")) {
					player.getInventory().addItem(Items.getDivorcePaper());
				}
			}
			
			else if (args[0].equalsIgnoreCase("breakup")) {
				Player player = Bukkit.getPlayer(args[1]);
				if (player == null) {
					sender.sendMessage(ChatColor.RED + "Đối phương không online");
					return false;
				}
				MarryDivorces.divorce(player);
			}
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendTut(sender);
		}
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("/marry get <proposal|engagement|divorce>");
		sender.sendMessage("/marry breakup <player>");
	}

}
