package mk.plugin.marry.data;

import org.bukkit.entity.Player;

import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class DataUtils {
	 
	public static PMData getData(Player player) {
		return getData(player.getName());
	}
	
	public static PMData getData(String player) {
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		String name = player;
		String partner = null;
		if (pd.hasData("marry-Partner")) partner = pd.getValue("marry-Partner");
		return new PMData(name, partner);
	}
	
}
