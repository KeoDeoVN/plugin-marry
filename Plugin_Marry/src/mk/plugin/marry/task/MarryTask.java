package mk.plugin.marry.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.marry.main.MainMarry;
import mk.plugin.marry.manager.MarryProposals;

public class MarryTask extends BukkitRunnable {

	@Override
	public void run() {
		
		MarryProposals.getProposals().forEach((player, prop) -> {
			if (prop.getTimeExpired() < System.currentTimeMillis()) {
				Bukkit.getScheduler().runTask(MainMarry.getPlugin(MainMarry.class), () -> {
					MarryProposals.refuse(player);
				});
			}
		});
	}

}
