package mk.plugin.marry.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import mk.plugin.marry.lang.Lang;
import mk.plugin.marry.util.MarryUtils;

public class MarryListener implements Listener {
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		if (!MarryUtils.isMarried(player)) return;
		String partner = MarryUtils.getPartner(player);
		String prefix = Lang.CHAT_PARTNER.get().replace("%player%", partner);
		e.setFormat(prefix + e.getFormat());
	}
	
}
