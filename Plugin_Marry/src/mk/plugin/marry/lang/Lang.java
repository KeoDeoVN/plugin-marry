package mk.plugin.marry.lang;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import mk.plugin.marry.yaml.YamlFile;

public enum Lang {
	
	PROPOSAL_TITLE("&c&l❤"),
	PROPOSAL_SUBTITLE("&a&oBạn có lời cầu hôn"),
	PROPOSAL_MESSAGE("&2&l>> &a%player% đã cầu hôn bạn với lời nhắn \"%message%\". &aGhi /emdongy để chấp nhận lời cầu hôn"),
	PROPOSAL_BROADCAST("&c&l❤❤❤ &a%player% đã cầu hôn %target%. Cùng chúc %player% may mắn nào"),
	PROSOSAL_NOT_HAVE("&cBạn không có lời cầu hôn nào"),
	
	PROPOSAL_SENDER_ALREADY_HAD("&cBạn đang có lời cầu hôn từ người khác"),
	PROPOSAL_TARGET_ALREADY_HAD("&cĐối phương đang có lời cầu hôn từ người khác"),
	PROPOSAL_SENDER_ALREADY_SENT("&cBạn đang cầu hôn người khác"),
	PROPOSAL_TARGET_ALREADY_SENT("&cĐối phương đang cầu hôn người khác"),
	
	PROPOSAL_REFUSE_TARGET_MESSAGE("&aBạn đã từ chối đối phương (bạn phũ vailon)"),
	PROPOSAL_REFUSE_SENDER_MESSAGE("&aBạn đã bị từ chối :(, Em đen lắm"),
	PROPOSAL_REFUSE_BROADCAST("&a%sender% đã từ chối %target%"),
	
	PROPOSAL_REFUSE_SENDER_TITLE("&7&lEm đen lắm"),
	PROPOSAL_REFUSE_SENDER_SUBTITLE("&6&oĐéo thể tin được, bạn bị từ chối"),
	
	PROPOSAL_YOURSELF("&c&lTự bucu à ?"),
	
	ENGAGEMENT_TITLE("&c&l❤❤❤"),
	ENGAGEMENT_SUBTITLE("&a&oChúc hai bạn trăm năm hạnh fuck"),
	ENGAGEMENT_MESSAGE("&2&l>> &aThành công! Giờ 2 bạn đã trở thành vợ chồng <3"),
	ENGAGEMENT_BROADCAST("&c&l❤❤❤ &a%player1% và %player2% đã chính thức thành vợ chồng <3"),
	
	DIVORCE_NOT_MARRIED("&cBạn chưa có vợ/chồng"),
	DIVORCE_TITLE("&7&l☹"),
	DIVORCE_SUBTITLE("&f&oHạnh phúc đổ vỡ"),
	DIVORCE_BROADCAST("&7&l❤❤❤ &aHạnh phúc giữa %player1% và %player2% đã tan vỡ"),
	
	MARRY_ALREADY("&cBạn có vợ/chồng rồi mà? Dcm định ngoại tình à"),
	
	CHAT_PARTNER("&f[&c❤%player%&f]&r "),
	
	;
	
	private String value;
	
	private Lang(String value) {
		this.value = value;
	}
	
	public String get() {
		return this.value.replace("&", "§");
	}
	
	public void set(String value) {
		this.value = value;
	}
	
	public String getName() {
		return this.name().toLowerCase().replace("_", "-").replace("&", "§");
	}
	
	public void send(Player player, Map<String, String> placeholders) {
		String s = this.get();
		for (Entry<String, String> e : placeholders.entrySet()) {
			s = s.replace(e.getKey(), e.getValue());
		}
		player.sendMessage(s);
	}
	
	public void send(Player player) {
		String s = this.get();
		player.sendMessage(s);
	}
	
	public void broadcast() {
		Bukkit.getOnlinePlayers().forEach(p -> {
			this.send(p);
		});
	}
	
	public void broadcast(String key, String value) {
		Map<String, String> m = Maps.newHashMap();
		m.put(key, value);
		Bukkit.getOnlinePlayers().forEach(p -> {
			this.send(p, m);
		});

	}
	
	public void send(Player player, String key, String value) {
		Map<String, String> placeholders = Maps.newHashMap();
		placeholders.put(key, value);
		send(player, placeholders);
	}
	
	public static void init(JavaPlugin plugin, YamlFile lang) {
		FileConfiguration config = lang.get();
		for (Lang l : Lang.values()) {
			if (config.contains(l.getName())) {
				l.set(config.getString(l.getName()).replace("&", "§"));
			}
			else {
				config.set(l.getName(), l.get().replace("§", "&"));
				lang.save(plugin);
			}
		}
	}
	
	public static void broadcast(String s) {
		Bukkit.getOnlinePlayers().forEach(p -> {
			p.sendMessage(s);
		});
	}
	
}
