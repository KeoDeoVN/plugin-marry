package mk.plugin.marry.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.marry.command.AdminCommand;
import mk.plugin.marry.command.PlayerCommand;
import mk.plugin.marry.config.Configs;
import mk.plugin.marry.lang.Lang;
import mk.plugin.marry.listener.MarryListener;
import mk.plugin.marry.task.MarryTask;
import mk.plugin.marry.yaml.YamlFile;

public class MainMarry extends JavaPlugin {
	
	@Override
	public void onEnable() {
		this.reloadConfigs();
		this.registerCommands();
		this.registerListeners();
		this.registerTasks();
	}
	
	public void reloadConfigs() {
		this.saveDefaultConfig();
		YamlFile.reloadAll(this);
		Lang.init(this, YamlFile.LANG);
		Configs.reload(YamlFile.CONFIG.get());
	}
	
	public void registerCommands() {
		this.getCommand("emdongy").setExecutor(new PlayerCommand());
		this.getCommand("cauhon").setExecutor(new PlayerCommand());
		this.getCommand("lyhon").setExecutor(new PlayerCommand());
		this.getCommand("emxinloi").setExecutor(new PlayerCommand());
		this.getCommand("marry").setExecutor(new AdminCommand());
	}
	
	public void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new MarryListener(), this);
	}
	
	public void registerTasks() {
		new MarryTask().runTaskTimer(this, 0, 10);
	}
	
}
