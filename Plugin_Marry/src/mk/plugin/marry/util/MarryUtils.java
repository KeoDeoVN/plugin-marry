package mk.plugin.marry.util;

import org.bukkit.entity.Player;

import mk.plugin.marry.data.DataUtils;
import mk.plugin.marry.data.PMData;

public class MarryUtils {
	
	public static boolean isMarried(Player player) {
		PMData pd = DataUtils.getData(player);
		return (pd.getPartner() != null);
	}
	
	public static String getPartner(Player player) {
		PMData pd = DataUtils.getData(player);
		return pd.getPartner();
	}
	
}
