package mk.plugin.marry.data;

import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class PMData {
	
	private String name;
	private String partner;
	
	public PMData(String name, String partner) {
		this.name = name;
		this.partner = partner;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPartner() {
		return this.partner;
	}
	
	public void setPartner(String name) {
		this.partner = name;
	}
	
	public void save() {
		PlayerData pd = PlayerDataAPI.getPlayerData(name);
		if (this.partner != null) pd.set("marry-Partner", partner);
		else pd.remove("marry-Partner");
		PlayerDataAPI.saveData(name);
	}
	
}
